import React, { Component } from 'react'
import Menu from 'components/menu'
import items from 'data/items'
import './App.scss'

class App extends Component {
	render() {
		return (
			<>
				<h1>Demo of an accessible menu!</h1>
				<Menu items={items} />
			</>
		)
	}
}

export default App
