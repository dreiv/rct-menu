import React, { Component } from 'react'
import cn from 'classnames'

import './Menu.scss'

class Menu extends Component {
	constructor(props) {
		super(props)

		this.handleKeyDown = this.handleKeyDown.bind(this)

		this.state = {
			toggled: false,
			cursor: 0,
		}
	}

	handleKeyDown = ({ key }) => {
		const { cursor, toggled } = this.state
		const { items } = this.props

		if (!toggled) {
			this.toggle(true)()
		}

		switch (key) {
			case 'ArrowUp':
			case 'ArrowLeft':
				this.setState(prevState => ({
					cursor: (prevState.cursor - 1 + items.length) % items.length,
				}))
				break
			case 'ArrowDown':
			case 'ArrowRight':
				this.setState(prevState => ({
					cursor: (prevState.cursor + 1) % items.length,
				}))
				break
			case 'Enter':
			case ' ':
				toggled ? this.onItemSelect(items[cursor])() : this.toggle(true)
				break
			case 'Escape':
				this.toggle(false)()
				break
			default:
				break
		}
	}

	toggle = isToggled => () => {
		this.setState({ toggled: isToggled })
	}

	onItemSelect = item => () => {
		console.log('item selected: ', item)
		this.toggle(false)()
	}

	renderItem = (item, index) => {
		const { cursor } = this.state
		const { id, name } = item

		return (
			<li
				key={id}
				onClick={this.onItemSelect(item)}
				className={cursor === index ? 'active' : null}
			>
				{name}
			</li>
		)
	}

	render() {
		const { toggled } = this.state
		const { items } = this.props

		const classes = cn('menu', { toggled })

		return (
			<div
				className={classes}
				tabIndex="0"
				onBlur={this.toggle(false)}
				onKeyDown={this.handleKeyDown}
			>
				<div className="trigger" onClick={this.toggle(true)}>
					…
				</div>
				<ul className="dropdown">{items.map(this.renderItem)}</ul>
			</div>
		)
	}
}

export default Menu
